/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        db = sqlitePlugin.openDatabase('mydbapp.db');
        remplissageTypeProduit();
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
    }
};

app.initialize();

function remplissageTypeProduit(){
    var $type_contrat = $('#Type_produit');
    $type_contrat.empty();
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM TYPE_PRODUIT', [], 
            function (tx, res) {
                var nb= JSON.stringify(res.rows.length);
                $type_contrat.append('<option value="0">-</option>');
                //$type_contrat.selectpicker('refresh');
                for(i=0; i<nb; i++){
                    //alert(res.rows.item(i).IDTYPEPRODUIT);
                    //alert(res.rows.item(i).LIBTYPEPRODUIT);
                    $type_contrat.append('<option value="'+ res.rows.item(i).IDTYPEPRODUIT +'">'+ res.rows.item(i).LIBTYPEPRODUIT+'</option>');
                } 

            },
            function(error){
                alertify.alert("Liste Type Produit non chargée.");
            });
    });
}

function remplissageProduit(){
    var $produit = $('#Produit');
    var typeProduitC = $('#Type_produit').val();
    //alert(typeProduitC);

    $produit.empty();
    
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PRODUIT WHERE IDTYPEPRODUIT = ?', [typeProduitC], 
            function (tx, res) {
                var nb= JSON.stringify(res.rows.length);
                $produit.append('<option value="0">-</option>');
                //$produit.selectpicker('refresh');
                for(i=0; i<nb; i++){
                    //alert(res.rows.item(i).CODEPRODUIT);
                    //alert(res.rows.item(i).LIBPRODUIT);
                    $produit.append('<option value="'+ res.rows.item(i).CODEPRODUIT +'">'+ res.rows.item(i).LIBPRODUIT+'</option>');
                } 

            },
            function(error){
                alertify.alert("Liste Produit non chargée.");
            });
    });
}

function product_choose(){
    var value_TypeProduct = $('#Type_produit').val()
    var value_product = $('#Produit').val();
    localStorage.setItem("CdProduit",value_product);
    //localStorage.removeItem("produit");
    if(value_product!=0){
        if(value_TypeProduct==1){
          document.location.href="souscription_epargne.html";
        }else if(value_TypeProduct==2){
          document.location.href="souscription_mixte.html";
        }else if(value_TypeProduct==3){
          document.location.href="souscription_rente.html";
        }else if(value_TypeProduct==4){
          document.location.href="souscription_deces.html";
        }else{

        }
    }else{
        document.getElementById("AZER05").style.display="block";
    } 
  }