// --------------------------------
// Date création : 11-12-2017 
// Date Dernière MAJ : 15-01-2018
// Auteur : Marie Colombe TRA LOU
// Parcours de souscription
// Gestion des champs de saisie
//---------------------------------


function Enable_QM_co_assure(){
	var el = document.getElementsByClassName("el_coassure"); 
         i = el.length;

    while(i--) {
        el[i].style.display = "block";
    }
}

function Disable_QM_co_assure(){
	var el = document.getElementsByClassName("el_coassure"); 
         i = el.length;

    while(i--) {
        el[i].style.display = "none";
    }
}

//-----------------------------------
// Insertion données - Récapitulatif
//-----------------------------------


// Affichage des informations du souscripteur (ou assuré)

function NomP_s(){
	//alert(document.getElementById("Nom_as").value);
	document.getElementById("NomP_s").innerHTML = document.getElementById("Nom_as").value;
}

function Dtn_s(){
	//alert(document.getElementById("Dtn_as").value);
	document.getElementById("Dtn_s").innerHTML = document.getElementById("Dtn_as").value;
}

function LieuNais_s(){
	//alert(document.getElementById("LieuNais_as").value);
	document.getElementById("LieuNais_s").innerHTML = document.getElementById("LieuNais_as").value;
}

function Sexe_s(){
	//alert(document.getElementById("Sexe_as").value);
	document.getElementById("Sexe_s").innerHTML = document.getElementById("Sexe_as").value;
}

function PieceId_s(){
	//alert(document.getElementById("PieceId_as").value);
	var valuePieceId = document.getElementById("PieceId_as").value;
	if(valuePieceId==1){
		document.getElementById("PieceId_s").innerHTML = "CNI";
	}else if(valuePieceId==2){
		document.getElementById("PieceId_s").innerHTML = "Passeport";
	}else if(valuePieceId==3){
		document.getElementById("PieceId_s").innerHTML = "Attestation d'identité";
	}else if(valuePieceId==4){
		document.getElementById("PieceId_s").innerHTML = "Permis de Conduire";
	}else {

	}	
}

function NumPiece_s(){
	//alert(document.getElementById("NumPiece_as").value);
	document.getElementById("NumPiece_s").innerHTML = document.getElementById("NumPiece_as").value;
}

function Prof_s(){
	//alert(document.getElementById("Prof_as").value);
	document.getElementById("Prof_s").innerHTML = document.getElementById("Prof_as").value;
}

function Emp_s(){
	//alert(document.getElementById("Emp_as").value);
	document.getElementById("Emp_s").innerHTML = document.getElementById("Emp_as").value;
}

function Adrs_s(){
	//alert(document.getElementById("Adrs_as").value);
	document.getElementById("Adrs_s").innerHTML = document.getElementById("Adrs_as").value;
}

function Mail_s(){
	//alert(document.getElementById("Mail_as").value);
	document.getElementById("Mail_s").innerHTML = document.getElementById("Mail_as").value;
}

function Tel_s(){
	//alert(document.getElementById("Tel_as").value);
	document.getElementById("Tel_s").innerHTML = document.getElementById("Tel_as").value;
}

// Affichage des informations sur le bénéficiaire

function NomP_bnf(num_el){
	//alert(document.getElementById("Nom_as").value);
	document.getElementById("NomP_bnf"+num_el).innerHTML = document.getElementById("Nom_bf"+num_el).value;
}

function Dtn_bnf(num_el){
	//alert(document.getElementById("Dtn_as").value);
	document.getElementById("Dtn_bnf"+num_el).innerHTML = document.getElementById("Dtn_bf"+num_el).value;
}

function LieuNaiss_bnf(num_el){
	//alert(document.getElementById("LieuNais_as").value);
	document.getElementById("LieuNaiss_bnf"+num_el).innerHTML = document.getElementById("LieuNaiss_bf"+num_el).value;
}

function Sexe_bnf(num_el){
	//alert(document.getElementById("Sexe_as").value);
	document.getElementById("Sexe_bnf"+num_el).innerHTML = document.getElementById("Sexe_bf"+num_el).value;
}

function Adrs_bnf(num_el){
	//alert(document.getElementById("Adrs_as").value);
	document.getElementById("Adrs_bnf"+num_el).innerHTML = document.getElementById("Adrs_bf"+num_el).value;
}

function Mail_bnf(num_el){
	//alert(document.getElementById("Mail_as").value);
	document.getElementById("Mail_bnf"+num_el).innerHTML = document.getElementById("Mail_bf"+num_el).value;
}

function Tel_bnf(num_el){
	//alert(document.getElementById("Tel_as").value);
	document.getElementById("Tel_bnf"+num_el).innerHTML = document.getElementById("Tel_bf"+num_el).value;
}

// Affichage des informations sur le contrat

/*function Prod_c(){
	document.getElementById("Prod_c").innerHTML = document.getElementById("Prod_ca").value;
}*/

function PrimEp_c(){
	document.getElementById("PrimEp_c").innerHTML = document.getElementById("PrimEp_ca").value;
}

function Period_c(){
	document.getElementById("Period_c").innerHTML = document.getElementById("Period_ca").value;
}

function PaieP_c(){
	document.getElementById("PaieP_c").innerHTML = document.getElementById("PaieP_ca").value;
}






//----------------------------------------
// Modification données - Récapitulatif
//----------------------------------------


// Modification des informations du souscripteur

function modif_NomP_s(){
	alertify
	  .defaultValue(document.getElementById("Nom_as").value)
	  .prompt("Modifier Nom & Prénoms Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("NomP_s").innerHTML = val ;
	      document.getElementById("Nom_as").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Dtn_s(){

	var message ='<label>Modifier Date de Naissance Assuré (aaaa-mm-jj) :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Dtn_m_as" type="date" name="dtn_m_as">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Dtn_s").innerHTML = document.getElementById("Dtn_m_as").value  ;
	   	document.getElementById("Dtn_as").value = document.getElementById("Dtn_m_as").value ;
	}, function() {
	    // user clicked "cancel"
	});
	   
}

function modif_LieuNais_s(){
	alertify
	  .defaultValue(document.getElementById("LieuNais_as").value)
	  .prompt("Modifier Lieu de Naissance Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("LieuNais_s").innerHTML = val ;
	      document.getElementById("LieuNais_as").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Sexe_s(){
	var message ='<label>Modifier Sexe Assuré :</label>'+
  				'<select class="form-control required" id="Sexe_m_as" type="text" name="sexe_m_as" style="height: 42px;"> '+
                  '<option value="M">Masculin</option>'+
                  '<option value="F">Féminin</option>'+
                '</select>';

  	alertify.confirm(message, function () {
  		document.getElementById("Sexe_s").innerHTML = document.getElementById("Sexe_m_as").value  ;
	   	document.getElementById("Sexe_as").value = document.getElementById("Sexe_m_as").value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_PieceId_s(){

	var message ='<label>Modifier Pièce d\'identité Assuré :</label>'+
  				'<select class="form-control required" id="PieceId_m_as" type="text" name="pieceId_m_as" style="height: 42px;"> '+
                	'<option value="1">CNI</option>'+
	                '<option value="2">Passeport</option>'+
	                '<option value="3">Attestation d\'identité</option>' +
	                '<option value="4">Permis de conduite</option>'+
                '</select>';

  	alertify.confirm(message, function () {
		var value_modif_PieceId = document.getElementById("PieceId_m_as").value;
		if(value_modif_PieceId==1){
			document.getElementById("PieceId_s").innerHTML = "CNI";
		}else if(value_modif_PieceId==2){
			document.getElementById("PieceId_s").innerHTML = "Passeport";
		}else if(value_modif_PieceId==3){
			document.getElementById("PieceId_s").innerHTML = "Attestation d'identité";
		}else if(value_modif_PieceId==4){
			document.getElementById("PieceId_s").innerHTML = "Permis de Conduire";
		}else {

		}	
		document.getElementById("PieceId_as").value = document.getElementById("PieceId_m_as").value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_NumPiece_s(){
	alertify
	  .defaultValue(document.getElementById("NumPiece_as").value)
	  .prompt("Modifier N° Pièce d'identité Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("NumPiece_s").innerHTML = val ;
	      document.getElementById("NumPiece_as").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Prof_s(){
	alertify
	  .defaultValue(document.getElementById("Prof_as").value)
	  .prompt("Modifier Profession Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Prof_s").innerHTML = val ;
	      document.getElementById("Prof_as").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Emp_s(){
	alertify
	  .defaultValue(document.getElementById("Emp_as").value)
	  .prompt("Modifier Employeur Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Emp_s").innerHTML = val ;
	      document.getElementById("Emp_as").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Adrs_s(){
	alertify
	  .defaultValue(document.getElementById("Adrs_as").value)
	  .prompt("Modifier Adresse Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Adrs_s").innerHTML = val ;
	      document.getElementById("Adrs_as").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Mail_s(){

	var message ='<label>Modifier Email Assuré :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Mail_m_as" type="email" name="mail_m_as"></div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Mail_s").innerHTML = document.getElementById("Mail_m_as").value  ;
	   	document.getElementById("Mail_as").value = document.getElementById("Mail_m_as").value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_Tel_s(){
	
	var message ='<label>Modifier Téléphone Assuré :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Tel_m_as" type="tel" name="tel_m_as"></div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Tel_s").innerHTML = document.getElementById("Tel_m_as").value  ;
	   	document.getElementById("Tel_as").value = document.getElementById("Tel_m_as").value ;
	}, function() {
	    // user clicked "cancel"
	});
}



// Modification des informations du co-assuré

function modif_NomP_cAs(){
	alertify
	  .defaultValue(document.getElementById("Nom_coAs").value)
	  .prompt("Modifier Nom & Prénoms Co-Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("NomP_cAs").innerHTML = val ;
	      document.getElementById("Nom_coAs").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Dtn_cAs(){

	message ='<label>Modifier Date de Naissance Co-Assuré (aaaa-mm-jj) :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Dtn_m_cAs" type="date" name="dtn_m_cAs">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Dtn_cAs").innerHTML = document.getElementById("Dtn_m_cAs").value  ;
	   	document.getElementById("Dtn_coAs").value = document.getElementById("Dtn_m_cAs").value ;
	}, function() {
	    // user clicked "cancel"
	});
	   
}

function modif_LieuNais_cAs(){
	alertify
	  .defaultValue(document.getElementById("LieuNais_coAs").value)
	  .prompt("Modifier Lieu de Naissance Co-Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("LieuNais_cAs").innerHTML = val ;
	      document.getElementById("LieuNais_CoAs").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Sexe_cAs(){
	message ='<label>Modifier Sexe Co-Assuré :</label>'+
  				'<select class="form-control required" id="Sexe_m_cAs" type="text" name="sexe_m_cAs" style="height: 42px;"> '+
                  '<option value="M">Masculin</option>'+
                  '<option value="F">Féminin</option>'+
                '</select>';

  	alertify.confirm(message, function () {
  		document.getElementById("Sexe_cAs").innerHTML = document.getElementById("Sexe_m_cAs").value  ;
	   	document.getElementById("Sexe_coAs").value = document.getElementById("Sexe_m_cAs").value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_PieceId_cAs(){

	message ='<label>Modifier Pièce d\'identité Co-Assuré :</label>'+
  				'<select class="form-control required" id="PieceId_m_cAs" type="text" name="pieceId_m_cAs" style="height: 42px;"> '+
                	'<option value="1">CNI</option>'+
	                '<option value="2">Passeport</option>'+
	                '<option value="3">Attestation d\'identité</option>' +
	                '<option value="4">Permis de conduite</option>'+
                '</select>';

  	alertify.confirm(message, function () {
		var value_modif_PieceId = document.getElementById("PieceId_m_cAs").value;
		if(value_modif_PieceId==1){
			document.getElementById("PieceId_cAs").innerHTML = "CNI";
		}else if(value_modif_PieceId==2){
			document.getElementById("PieceId_cAs").innerHTML = "Passeport";
		}else if(value_modif_PieceId==3){
			document.getElementById("PieceId_cAs").innerHTML = "Attestation d'identité";
		}else if(value_modif_PieceId==4){
			document.getElementById("PieceId_cAs").innerHTML = "Permis de Conduire";
		}else {

		}	
		document.getElementById("PieceId_coAs").value = document.getElementById("PieceId_m_cAs").value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_NumPiece_cAs(){
	alertify
	  .defaultValue(document.getElementById("NumPiece_coAs").value)
	  .prompt("Modifier N° Pièce d'identité Co-Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("NumPiece_cAs").innerHTML = val ;
	      document.getElementById("NumPiece_coAs").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Prof_cAs(){
	alertify
	  .defaultValue(document.getElementById("Prof_coAs").value)
	  .prompt("Modifier Profession Co-Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Prof_cAs").innerHTML = val ;
	      document.getElementById("Prof_coAs").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Emp_cAs(){
	alertify
	  .defaultValue(document.getElementById("Emp_coAs").value)
	  .prompt("Modifier Employeur Co-Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Emp_cAs").innerHTML = val ;
	      document.getElementById("Emp_coAs").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Adrs_cAs(){
	alertify
	  .defaultValue(document.getElementById("Adrs_coAs").value)
	  .prompt("Modifier Adresse Co-Assuré :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Adrs_cAs").innerHTML = val ;
	      document.getElementById("Adrs_coAs").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Mail_cAs(){

	message ='<label>Modifier Email Co-Assuré :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Mail_m_cAs" type="text" name="mail_m_cAs">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Mail_cAs").innerHTML = document.getElementById("Mail_m_cAs").value  ;
	   	document.getElementById("Mail_coAs").value = document.getElementById("Mail_m_cAs").value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_Tel_cAs(){
	
	message ='<label>Modifier Téléphone Co-Assuré :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Tel_m_cAs" type="text" name="tel_m_cAs">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Tel_cAs").innerHTML = document.getElementById("Tel_m_cAs").value  ;
	   	document.getElementById("Tel_coAs").value = document.getElementById("Tel_m_cAs").value ;
	}, function() {
	    // user clicked "cancel"
	});
}



// Modification des informations du bénéficiaire

function beneficiaire_r_Click(){
	if (document.getElementById("Ayant_droit_r").checked == true){
		alertify.confirm("Voulez vous annuler le(s) bénéficiaire(s) désigné(s) ?", function () {
            // user clicked "ok"
            document.getElementById("beneficiaire_r_block").style.display = "none";

            annulBeneficiaire(1);
            annulBeneficiaire(2);
            annulBeneficiaire(3);

			document.getElementById("Ayant_droit").checked = true;
			document.getElementById("beneficiaire_block").style.display = "none";
        }, function() {
            // user clicked "cancel"
            document.getElementById("Ayant_droit_r").checked = false;
        });    
	  
	}else{
	  document.getElementById("beneficiaire_r_block").style.display = "block";
	  document.getElementById("Ayant_droit").checked = false;
	  document.getElementById("beneficiaire_block").style.display = "block";
	}
}

function modif_NomP_bnf(num_el){
	alertify
	  .defaultValue(document.getElementById("Nom_bf"+num_el).value)
	  .prompt("Modifier Nom & Prénoms bénéficiaire "+num_el+" :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("NomP_bnf"+num_el).innerHTML = val ;
	      document.getElementById("Nom_bf"+num_el).value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Dtn_bnf(num_el){

	message ='<label>Modifier Date de Naissance Bénéficiaire '+num_el+' (aaaa-mm-jj) :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Dtn_m_bf'+num_el+'" type="date" name="dtn_m_bf'+num_el+'">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Dtn_bnf"+num_el).innerHTML = document.getElementById("Dtn_m_bf"+num_el).value  ;
	   	document.getElementById("Dtn_bf"+num_el).value = document.getElementById("Dtn_m_bf"+num_el).value ;
	}, function() {
	    // user clicked "cancel"
	});
	   
}

function modif_LieuNaiss_bnf(num_el){
	alertify
	  .defaultValue(document.getElementById("LieuNaiss_bf"+num_el).value)
	  .prompt("Modifier Lieu de Naissance Bénéficiaire "+num_el+" :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("LieuNaiss_bnf"+num_el).innerHTML = val ;
	      document.getElementById("LieuNaiss_bf"+num_el).value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}


/*function modif_NomP_bnf(num_el){
	alertify
	  .defaultValue(document.getElementById("Nom_bf"+num_el).value)
	  .prompt("Modifier Nom & Prénoms bénéficiaire "+num_el+" :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("NomP_bnf"+num_el).innerHTML = val ;
	      document.getElementById("Nom_bf"+num_el).value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}*/

function modif_Sexe_bnf(num_el){
	message ='<label>Modifier Sexe Bénéficiaire '+num_el+' :</label>'+
  				'<select class="form-control required" id="Sexe_m_bf'+num_el+'" type="text" name="sexe_m_bf'+num_el+'" style="height: 42px;"> '+
                  '<option value="M">Masculin</option>'+
                  '<option value="F">Féminin</option>'+
                '</select>';

  	alertify.confirm(message, function () {
  		document.getElementById("Sexe_bnf"+num_el).innerHTML = document.getElementById("Sexe_m_bf"+num_el).value  ;
	   	document.getElementById("Sexe_bf"+num_el).value = document.getElementById("Sexe_m_bf"+num_el).value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_Adrs_bnf(num_el){
	alertify
	  .defaultValue(document.getElementById("Adrs_bf"+num_el).value)
	  .prompt("Modifier Adresse Bénéficiaire "+num_el+" :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Adrs_bnf"+num_el).innerHTML = val ;
	      document.getElementById("Adrs_bf"+num_el).value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Mail_bnf(num_el){

	message ='<label>Modifier Email Bénéficiaire '+num_el+' :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Mail_m_bf'+num_el+'" type="email" name="mail_m_bf'+num_el+'">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Mail_bnf"+num_el).innerHTML = document.getElementById("Mail_m_bf"+num_el).value  ;
	   	document.getElementById("Mail_bf"+num_el).value = document.getElementById("Mail_m_bf"+num_el).value ;
	}, function() {
	    // user clicked "cancel"
	});
}

function modif_Tel_bnf(num_el){
	
	message ='<label>Modifier Téléphone Bénéficiaire '+num_el+':</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Tel_m_bf'+num_el+'" type="tel" name="tel_m_bf'+num_el+'">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("Tel_bnf"+num_el).innerHTML = document.getElementById("Tel_m_bf"+num_el).value  ;
	   	document.getElementById("Tel_bf"+num_el).value = document.getElementById("Tel_m_bf"+num_el).value ;
	}, function() {
	    // user clicked "cancel"
	});
}

// Modification des informations du contrat

function modif_PrimEp_c(){
	alertify
	  .defaultValue(document.getElementById("PrimEp_c").value)
	  .prompt("Modifier Montant Prime Epargne :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("PrimEp_c").innerHTML = val ;
	      document.getElementById("PrimEp_ca").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );
}

function modif_Period_c(){
	var message ='<label>Modifier Périodicité :</label>'+
  				'<select class="form-control required" id="Period_m" type="text" name="period_m" style="height: 42px;"> '+
	                '<option value="M">Mensuelle</option>'+
	                '<option value="T">Trismestrielle</option>'+
	                '<option value="S">Semestrielle</option>'+
	                '<option value="A">Annuelle</option>'+
                '</select>';
    alertify.confirm(message, function () {
  		document.getElementById("Period_c").innerHTML = document.getElementById("Period_m").value  ;
	   	document.getElementById("Period_ca").value = document.getElementById("Period_m").value ;
	}, function() {
	    // user clicked "cancel"
	});

	/*alertify
	  .defaultValue(document.getElementById("Period_c").value)
	  .prompt("Modifier Périodicité :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("Period_c").innerHTML = val ;
	      document.getElementById("Period_ca").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );*/
}

function modif_PaieP_c(){
	var message ='<label>Modifier Paiement Privilégié :</label>'+
  				'<select class="form-control required" id="PaieP_m" type="text" name="PaieP_m" style="height: 42px;"> '+
	                '<option value="CHQ">Chèque</option>'+
                    '<option value="ESP">Espèces</option>'+
                    '<option value="VIR">Virement bancaire</option>'+
                '</select>';
    alertify.confirm(message, function () {
  		document.getElementById("PaieP_c").innerHTML = document.getElementById("PaieP_m").value  ;
	   	document.getElementById("PaieP_ca").value = document.getElementById("PaieP_m").value ;
	}, function() {
	    // user clicked "cancel"
	});

	/*alertify
	  .defaultValue(document.getElementById("PaieP_c").value)
	  .prompt("Modifier Paiement Privilégié :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("PaieP_c").innerHTML = val ;
	      document.getElementById("PaieP_ca").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );*/
}

function modif_DatEf_c(){
	/*alertify
	  .defaultValue(document.getElementById("DatEf_ca").value)
	  .prompt("Modifier Paiement Privilégié :",
	    function (val, ev) {
	      ev.preventDefault();
	      //alertify.success("You've clicked OK and typed: " + val);
	      document.getElementById("PaieP_c").innerHTML = val ;
	      document.getElementById("PaieP_ca").value = val ;
	    }, function(ev) {
	      //ev.preventDefault();
	      //alertify.error("You've clicked Cancel");
	    }
	  );*/

	var message ='<label>Modifier Date d\'effet (aaaa-mm-jj) :</label>'+
  				'<div class="input-group input-group-lg">'+
	            	'<input class="form-control required" id="Dtn_m" type="date" name="dtn_m">'+
	          	'</div>';

  	alertify.confirm(message, function () {
  		document.getElementById("DatEf_c").innerHTML = document.getElementById("Dtn_m").value  ;
	}, function() {
	    // user clicked "cancel"
	});
}