/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var pictureSource;   // picture source
var destinationType; // sets the format of returned value
var imageDefaultData = "assets/img/icon/pj.png";




var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');

        codeProduit = localStorage.getItem('CdProduit');

        db = sqlitePlugin.openDatabase('mydbapp.db');

        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;

        listePiece();
        //remplissageMPaie();
        //remplissagePeriodicite();
        loadParamEpargne();
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
    }
};

app.initialize();

function listePiece(){
    var $piece = $('#PieceId_as');
    $piece.empty();
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PIECE_IDENTITE', [], 
            function (tx, res) {
                var nb= JSON.stringify(res.rows.length);
                $piece.append('<option value="0">-</option>');
                //$type_contrat.selectpicker('refresh');
                for(i=0; i<nb; i++){
                    $piece.append('<option value="'+ res.rows.item(i).IDPIECE +'">'+ res.rows.item(i).LIBPIECE+'</option>');
                } 

            },
            function(error){
                alertify.alert("Liste pièce d'identité non chargée.");
            });
    });
}

function remplissageMPaie(){}

function replissagePeriodicite(){}

function loadParamEpargne(){
    var frais = "";
    var devise = "";
    // Chargement Frais d'entrée
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 1], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Frais_adhesion').innerHTML = res.rows.item(0).VALEURPARAM;
                frais = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Frais d'adhésion non chargée.");
            });
    });

    //Chargement Taux de gestion
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 2], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Taux_gestion').innerHTML = res.rows.item(0).VALEURPARAM;
                document.getElementById('FraisGest_c').innerHTML = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Taux de gestion non chargée.");
            });
    });

    //Chargement Taux Encours gérés
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 3], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Taux_encours').innerHTML = res.rows.item(0).VALEURPARAM;
                document.getElementById('FraisEnc_c').innerHTML = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Taux Encours non chargée.");
            });
    });

    //Chargement Dévise Pays
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PAYS WHERE CODEPAYS = ?', [225], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Devise_pays').innerHTML = res.rows.item(0).DEVISEPAYS;
                devise = res.rows.item(0).DEVISEPAYS;
            },
            function(error){
                alertify.alert("Dévise pays non chargée.");
            });
    }); 

    //Chargement Nom produit
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PRODUIT WHERE CODEPRODUIT = ?', [codeProduit], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Nom_produit').innerHTML = res.rows.item(0).LIBPRODUIT;
                document.getElementById('Prod_c').innerHTML = res.rows.item(0).LIBPRODUIT;
            },
            function(error){
                alertify.alert("Nom produit non chargée.");
            });
    });

    //Chargement Age Min
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 5], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                ageMin = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Age Minimal non chargée.");
            });
    });

    //Chargement Age Max
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 6], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                ageMax = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Age Maximal non chargée.");
            });
    });

    // Frais d'adhésion + dévise
    document.getElementById('FraisAdh_c').innerHTML = frais + " "+devise;
}

function pieceJointe1(){
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess1, onFail, { quality: 50, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}


function pieceJointe2(){
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess2, onFail, { quality: 50, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}

function onPhotoDataSuccess1(imageData) {
    // Uncomment to view the base64-encoded image data
    // console.log(imageData);

    // Get image handle
    //
    var smallImage = document.getElementById('Pj1');
    var smallImage_r = document.getElementById('Pj1_r');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = "data:image/jpeg;base64," + imageData;
    smallImage_r.src = "data:image/jpeg;base64," + imageData;

    document.getElementById("delete_Pj1").style.display="block";
}

function onPhotoDataSuccess2(imageData) {
    // Uncomment to view the base64-encoded image data
    // console.log(imageData);

    // Get image handle
    //
    var smallImage = document.getElementById('Pj2');
    var smallImage_r = document.getElementById('Pj2_r');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = "data:image/jpeg;base64," + imageData;
    smallImage_r.src = "data:image/jpeg;base64," + imageData;

    document.getElementById("delete_Pj2").style.display="block";
}

function deletePieceJointe1(){

     var smallImage = document.getElementById('Pj1');
     var smallImage_r = document.getElementById('Pj1_r');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = imageDefaultData;
    smallImage_r.src = imageDefaultData;

    document.getElementById("delete_Pj1").style.display="none";
}

function deletePieceJointe2(){

     var smallImage = document.getElementById('Pj2');
     var smallImage_r = document.getElementById('Pj2_r');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = imageDefaultData;
    smallImage_r.src = imageDefaultData;

    document.getElementById("delete_Pj2").style.display="none";
}

// Called if something bad happens.
//
function onFail(message) {
    alertify.alert('AZER06: ' + message);
}


function ajoutBeneficiaire(){
    /* variable ID beneficiaire */
    var blockBeneficiaire1 = document.getElementById('Beneficiaire_1');         //B1
    var enteteBlockBeneficiare1 = document.getElementById('Entete_bf1');        //Entete B1
    var blockBeneficiaire2 = document.getElementById('Beneficiaire_2');         //B2
    var blockBeneficiaire3 = document.getElementById('Beneficiaire_3');         //B3
    var iconAjoutBeneficiare = document.getElementById('Ajout_beneficiaire');   //B_PNG
    /* variable ID beneficiaire - Recapitulatif */
    var blockBeneficiaire1_r = document.getElementById('Bf_1');                     //B1
    var enteteBlockBeneficiare1_r = document.getElementById('Entete_bnf1');         //Entete B1
    var blockBeneficiaire2_r = document.getElementById('Bf_2');                     //B2
    var blockBeneficiaire3_r = document.getElementById('Bf_3');                     //B3
    var iconAjoutBeneficiare_r = document.getElementById('Ajout_beneficiaire_r');   //B_PNG
    /* variable icon de modification beneficiaire 1 - Recapitulatif */
    var iconModifNompBf = document.getElementById('icon_modif_NomP_bnf1');
    var iconModifDtnBf = document.getElementById('icon_modif_Dtn_bnf1');
    var iconModifLieuNaissBf = document.getElementById('icon_modif_LieuNaiss_bnf1');
    var iconModifSexeBf = document.getElementById('icon_modif_Sexe_bnf1');
    var iconModifMailBf = document.getElementById('icon_modif_Mail_bnf1');
    var iconModifTelBf = document.getElementById('icon_modif_Tel_bnf1');
    var iconModifAdrsBf = document.getElementById('icon_modif_Adrs_bnf1');


    // Si B1 actif - B2 inactif - B3 inactif (condition effectuée sur entête B1)
    if(enteteBlockBeneficiare1.style.display=="none"){
        //Activer Entête B1 - B2
        enteteBlockBeneficiare1.style.display="block";
        blockBeneficiaire2.style.display="block";
        //Activer Entête B1 - B2 (recapitulatif)
        enteteBlockBeneficiare1_r.style.display="block";
        blockBeneficiaire2_r.style.display="block";
        iconModifNompBf.style.visibility = "hidden";
        iconModifDtnBf.style.visibility = "hidden";
        iconModifLieuNaissBf.style.visibility = "hidden";
        iconModifSexeBf.style.visibility = "hidden";
        iconModifMailBf.style.visibility = "hidden";
        iconModifTelBf.style.visibility = "hidden";
        iconModifAdrsBf.style.visibility = "hidden";
    }
    // Si B1 actif - B2 actif
    else {
        if((enteteBlockBeneficiare1.style.display=="block") && (blockBeneficiaire2.style.display=="block")){
            //Activer B3
            blockBeneficiaire3.style.display="block";
            iconAjoutBeneficiare.style.display="none";
            //Activer B3 (recapitulatif)
            blockBeneficiaire3_r.style.display="block";
        }
    }
}

function annulBeneficiaire(num_el){
    // variables : Saisie de bénéficiaire
    var nompBf = document.getElementById('Nom_bf'+num_el);
    var sexeBf = document.getElementById('Sexe_bf'+num_el);
    var dtnBf = document.getElementById('Dtn_bf'+num_el);
    var lieunaissBf = document.getElementById('LieuNaiss_bf'+num_el);
    var mailBf = document.getElementById('Mail_bf'+num_el);
    var adressBf = document.getElementById('Adrs_bf'+num_el);
    var telBf = document.getElementById('Tel_bf'+num_el);

    // variables : Récapitulatif bénéficiaire
    var nompBnf = document.getElementById('NomP_bnf'+num_el);
    var sexeBnf = document.getElementById('Sexe_bnf'+num_el);
    var dtnBnf = document.getElementById('Dtn_bnf'+num_el);
    var lieunaissBnf = document.getElementById('LieuNaiss_bnf'+num_el);
    var mailBnf = document.getElementById('Mail_bnf'+num_el);
    var adressBnf = document.getElementById('Adrs_bnf'+num_el);
    var telBnf = document.getElementById('Tel_bnf'+num_el);

    // nom
    nompBf.value = "";
    // sexe
    sexeBf.value = "0";
    // date de naissance
    dtnBf.value = "";
    // lieu de naissance
    lieunaissBf.value = "";
    // email
    mailBf.value = "";
    // adresse
    adressBf.value = "";
    // N° Téléphone
    telBf.value = "";

    // nom (Recapitulatif)
    nompBnf.innerHTML = "Non défini";
    // sexe (Recapitulatif)
    sexeBnf.innerHTML = "Non défini";
    // date de naissance (Recapitulatif)
    dtnBnf.innerHTML = "Non défini";
    // lieu de naissance (Recapitulatif)
    lieunaissBnf.innerHTML = "Non défini";
    // email (Recapitulatif)
    mailBnf.innerHTML = "Non défini";
    // adresse (Recapitulatif)
    adressBnf.innerHTML = "Non défini";
    // N° Téléphone (Recapitulatif)
    telBnf.innerHTML = "Non défini";
}


function deleteBeneficiaire(num_el){
    /* variable ID beneficiaire */
    var blockBeneficiaire1 = document.getElementById('Beneficiaire_1');         //B1
    var enteteBlockBeneficiaire1 = document.getElementById('Entete_bf1');        //Entete B1
    var blockBeneficiaire2 = document.getElementById('Beneficiaire_2');         //B2
    var blockBeneficiaire3 = document.getElementById('Beneficiaire_3');         //B3
    var iconAjoutBeneficiare = document.getElementById('Ajout_beneficiaire');   //B_PNG
    /* variable ID beneficiaire - Recapitulatif */
    var blockBeneficiaire1_r = document.getElementById('Bf_1');                     //B1
    var enteteBlockBeneficiaire1_r = document.getElementById('Entete_bnf1');         //Entete B1
    var blockBeneficiaire2_r = document.getElementById('Bf_2');                     //B2
    var blockBeneficiaire3_r = document.getElementById('Bf_3');                     //B3
    var iconAjoutBeneficiare_r = document.getElementById('Ajout_beneficiaire_r');   //B_PNG
    
    /* variable icon de modification beneficiaire 1 - Recapitulatif */
    var iconModifNompBf = document.getElementById('icon_modif_NomP_bnf1');
    var iconModifDtnBf = document.getElementById('icon_modif_Dtn_bnf1');
    var iconModifLieuNaissBf = document.getElementById('icon_modif_LieuNaiss_bnf1');
    var iconModifSexeBf = document.getElementById('icon_modif_Sexe_bnf1');
    var iconModifMailBf = document.getElementById('icon_modif_Mail_bnf1');
    var iconModifTelBf = document.getElementById('icon_modif_Tel_bnf1');
    var iconModifAdrsBf = document.getElementById('icon_modif_Adrs_bnf1');

    /* variable beneficiaire 1 */
    var nompBf1 = document.getElementById('Nom_bf1');
    var sexeBf1 = document.getElementById('Sexe_bf1');
    var dtnBf1 = document.getElementById('Dtn_bf1');
    var lieunaissBf1 = document.getElementById('LieuNaiss_bf1');
    var mailBf1 = document.getElementById('Mail_bf1');
    var adressBf1 = document.getElementById('Adrs_bf1');
    var telBf1 = document.getElementById('Tel_bf1');
    /* variable beneficiare 2 */
    var nompBf2 = document.getElementById('Nom_bf2');
    var sexeBf2 = document.getElementById('Sexe_bf2');
    var dtnBf2 = document.getElementById('Dtn_bf2');
    var lieunaissBf2 = document.getElementById('LieuNaiss_bf2');
    var mailBf2 = document.getElementById('Mail_bf2');
    var adressBf2 = document.getElementById('Adrs_bf2');
    var telBf2 = document.getElementById('Tel_bf2');
    /* variable beneficiare 3 */
    var nompBf3 = document.getElementById('Nom_bf3');
    var sexeBf3 = document.getElementById('Sexe_bf3');
    var dtnBf3 = document.getElementById('Dtn_bf3');
    var lieunaissBf3 = document.getElementById('LieuNaiss_bf3');
    var mailBf3 = document.getElementById('Mail_bf3');
    var adressBf3 = document.getElementById('Adrs_bf3');
    var telBf3 = document.getElementById('Tel_bf3');

    /* variable beneficiaire 1 - Recapitulatif */
    var nompBnf1 = document.getElementById('NomP_bnf1');
    var sexeBnf1 = document.getElementById('Sexe_bnf1');
    var dtnBnf1 = document.getElementById('Dtn_bnf1');
    var lieunaissBnf1 = document.getElementById('LieuNaiss_bnf1');
    var mailBnf1 = document.getElementById('Mail_bnf1');
    var adressBnf1 = document.getElementById('Adrs_bnf1');
    var telBnf1 = document.getElementById('Tel_bnf1');
    /* variable beneficiare 2 - Recapitulatif */
    var nompBnf2 = document.getElementById('NomP_bnf2');
    var sexeBnf2 = document.getElementById('Sexe_bnf2');
    var dtnBnf2 = document.getElementById('Dtn_bnf2');
    var lieunaissBnf2 = document.getElementById('LieuNaiss_bnf2');
    var mailBnf2 = document.getElementById('Mail_bnf2');
    var adressBnf2 = document.getElementById('Adrs_bnf2');
    var telBnf2 = document.getElementById('Tel_bnf2');
    /* variable beneficiare 3 - Recapitulatif */
    var nompBnf3 = document.getElementById('NomP_bnf3');
    var sexeBnf3 = document.getElementById('Sexe_bnf3');
    var dtnBnf3 = document.getElementById('Dtn_bnf3');
    var lieunaissBnf3 = document.getElementById('LieuNaiss_bnf3');
    var mailBnf3 = document.getElementById('Mail_bnf3');
    var adressBnf3 = document.getElementById('Adrs_bnf3');
    var telBnf3 = document.getElementById('Tel_bnf3');

    //Suppression Beneficiaire 1
    if(num_el==1){
        //Si B2 actif - B3 actif 
        if((blockBeneficiaire2.style.display=="block") && (blockBeneficiaire3.style.display=="block")){
            alertify.confirm("Voulez vous supprimer le bénéficiare 1 ?", function () {
                // L'utilisateur clique sur OUI
                // nom
                nompBf1.value = nompBf2.value;
                nompBf2.value = nompBf3.value;
                // sexe
                sexeBf1.value = sexeBf2.value;
                sexeBf2.value = sexeBf3.value;
                // date de naissance
                dtnBf1.value = dtnBf2.value;
                dtnBf2.value = dtnBf3.value;
                // lieu de naissance
                lieunaissBf1.value = lieunaissBf2.value;
                lieunaissBf2.value = lieunaissBf3.value;
                // email
                mailBf1.value = mailBf2.value;
                mailBf2.value = mailBf3.value;
                // adresse
                adressBf1.value = adressBf2.value;
                adressBf2.value = adressBf3.value;
                // N° Téléphone
                telBf1.value = telBf2.value;
                telBf2.value = telBf3.value;

                // nom - Recapitulatif
                nompBnf1.innerHTML = nompBf1.value
                nompBnf2.innerHTML = nompBf2.value;
                // sexe - Recapitulatif
                sexeBnf1.innerHTML = sexeBf1.value;
                sexeBnf2.innerHTML = sexeBf2.value;
                // date de naissance - Recapitulatif
                dtnBnf1.innerHTML = dtnBf1.value;
                dtnBnf2.innerHTML = dtnBf2.value
                // lieu de naissance - Recapitulatif
                lieunaissBnf1.innerHTML = lieunaissBf1.value;
                lieunaissBnf2.innerHTML = lieunaissBf2.valuet;
                // email - Recapitulatif
                mailBnf1.innerHTML = mailBf1.value;
                mailBnf2.innerHTML = mailBf2.value;
                // adresse - Recapitulatif
                adressBnf1.innerHTML = adressBf1.value;
                adressBnf2.innerHTML = adressBf2.value;
                // N° Téléphone - Recapitulatif
                telBnf1.innerHTML = telBf1.value;
                telBnf2.innerHTML = telBf2.value;

                annulBeneficiaire(3);

                blockBeneficiaire3.style.display="none";
                blockBeneficiaire1_r.style.display="none";
                iconAjoutBeneficiare.style.display="block";

            }, function() {
                // L'utilisateur clique sur NON
                
            }); 

        }else if((blockBeneficiaire2.style.display=="block") && (blockBeneficiaire3.style.display=="none")){
            alertify.confirm("Voulez vous supprimer le bénéficiare 1 ?", function () {
                // L'utilisateur clique sur OUI
                // nom
                nompBf1.value = nompBf2.value;
                // sexe
                sexeBf1.value = sexeBf2.value;
                // date de naissance
                dtnBf1.value = dtnBf2.value;
                // lieu de naissance
                lieunaissBf1.value = lieunaissBf2.value;
                // email
                mailBf1.value = mailBf2.value;
                // adresse
                adressBf1.value = adressBf2.value;
                // N° Téléphone
                telBf1.value = telBf2.value;

                // nom - Recapitulatif
                nompBnf1.innerHTML = nompBf1.value;
                // sexe - Recapitulatif
                sexeBnf1.innerHTML = sexeBf1.value;
                // date de naissance - Recapitulatif
                dtnBnf1.innerHTML = dtnBf1.value
                // lieu de naissance - Recapitulatif
                lieunaissBnf1.innerHTML = lieunaissBf1.value;
                // email - Recapitulatif
                mailBnf1.innerHTML = mailBf1.value;
                // adresse - Recapitulatif
                adressBnf1.innerHTML = adressBf1.value;
                // N° Téléphone - Recapitulatif
                telBnf1.innerHTML = telBf1.value;

                annulBeneficiaire(2);

                blockBeneficiaire2.style.display="none";
                blockBeneficiaire2_r.style.display="none";
                enteteBlockBeneficiaire1.style.display="none";
                enteteBlockBeneficiaire1_r.style.display="none";

                iconModifNompBf.style.visibility = "visible";
                iconModifDtnBf.style.visibility = "visible";
                iconModifLieuNaissBf.style.visibility = "visible";
                iconModifSexeBf.style.visibility = "visible";
                iconModifMailBf.style.visibility = "visible";
                iconModifTelBf.style.visibility = "visible";
                iconModifAdrsBf.style.visibility = "visible";

            }, function() {
                // L'utilisateur clique sur NON
                
            }); 

        }else{}
  
    }else if(num_el==2){
        //Si B1 actif - B2 actif - B3 actif 
        if(blockBeneficiaire3.style.display=="block"){
            alertify.confirm("Voulez vous supprimer le bénéficiare 2 ?", function () {
                // L'utilisateur clique sur OUI
                // nom
                nompBf2.value = nompBf3.value;
                // sexe
                sexeBf2.value = sexeBf3.value;
                // date de naissance
                dtnBf2.value = dtnBf3.value;
                // lieu de naissance
                lieunaissBf2.value = lieunaissBf3.value;
                // email
                mailBf2.value = mailBf3.value;
                // adresse
                adressBf2.value = adressBf3.value;
                // N° Téléphone
                telBf2.value = telBf3.value;

                // nom - Recapitulatif
                nompBnf2.innerHTML = nompBf2.value ;
                // sexe - Recapitulatif
                sexeBnf2.innerHTML = sexeBf2.value ;
                // date de naissance - Recapitulatif
                dtnBnf2.innerHTML = dtnBf2.value ;
                // lieu de naissance - Recapitulatif
                lieunaissBnf2.innerHTML = lieunaissBf2.value ;
                // email - Recapitulatif
                mailBnf2.innerHTML = mailBf2.value ;
                // adresse - Recapitulatif
                adressBnf2.innerHTML = adressBf2.value;
                // N° Téléphone - Recapitulatif
                telBnf2.innerHTML = telBf2.value;

                annulBeneficiaire(3);

                blockBeneficiaire3.style.display="none";
                blockBeneficiaire3_r.style.display="none";
                iconAjoutBeneficiare.style.display="block";

            }, function() {
                // L'utilisateur clique sur NON
                
            }); 

        }
        // Si B1 actif - B2 actif - B3 inactif
        else {
            alertify.confirm("Voulez vous supprimer le bénéficiare 2 ?", function () {
                // L'utilisateur clique sur OUI
                annulBeneficiaire(2);

                blockBeneficiaire2.style.display="none";
                blockBeneficiaire2_r.style.display="none";
                enteteBlockBeneficiaire1.style.display="none";
                enteteBlockBeneficiaire1_r.style.display="none";

            }, function() {
                // L'utilisateur clique sur NON
                
            }); 
        }

    }else if(num_el==3){
        // B1 actif - B2 actif - B3 actif
        alertify.confirm("Voulez vous supprimer le bénéficiare 3 ?", function () {
            // L'utilisateur clique sur OUI
            annulBeneficiaire(3);

            blockBeneficiaire3.style.display="none";
            blockBeneficiaire3_r.style.display="none";
            iconAjoutBeneficiare.style.display="block";

        }, function() {
            // L'utilisateur clique sur NON
            
        }); 
    }else{

    }

    
}

/*function openCamera(selection) {
    var srcType = Camera.PictureSourceType.CAMERA;
    var options = setOptions(srcType);
    var func = createNewFileEntry;

    if (selection == "camera-thmb") {
        options.targetHeight = 200;
        options.targetWidth = 200;
    }

    navigator.camera.getPicture(function cameraSuccess(imageUri) {
        document.getElementById("Pj1").src=imageUri;
        // Do something

    }, function cameraError(error) {
        console.debug("Unable to obtain picture: " + error, "app");

    }, options);
}

function openFilePicker(selection) {
    var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
    var options = setOptions(srcType);
    var func = createNewFileEntry;

    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        // Do something

    }, function cameraError(error) {
        console.debug("Unable to obtain picture: " + error, "app");

    }, options);
}*/
