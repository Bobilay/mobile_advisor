/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/* variable capture PJ */
var pictureSource;   // picture source
var destinationType; // sets the format of returned value
var imageDefaultData = "assets/img/icon/pj.png";

/* variable ID beneficiaire */
var blockBeneficiaire1 = document.getElementById('Beneficiaire_1');         //B1
var enteteBlockBeneficiare1 = document.getElementById('Entete_bf1');        //Entete B1
var blockBeneficiaire2 = document.getElementById('Beneficiaire_2');         //B2
var blockBeneficiaire3 = document.getElementById('Beneficiaire_3');         //B3
var iconAjoutBeneficiare = document.getElementById('Ajout_beneficiaire');   //B_PNG

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');

        codeProduit = localStorage.getItem('CdProduit');

        db = sqlitePlugin.openDatabase('mydbapp.db');

        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;

        listePiece();
        //remplissageMPaie();
        //remplissagePeriodicite();
        loadParamEpargne();
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
    }
};

app.initialize();

function listePiece(){
    var $piece = $('#PieceId_as');
    $piece.empty();
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PIECE_IDENTITE', [], 
            function (tx, res) {
                var nb= JSON.stringify(res.rows.length);
                $piece.append('<option value="0">-</option>');
                //$type_contrat.selectpicker('refresh');
                for(i=0; i<nb; i++){
                    $piece.append('<option value="'+ res.rows.item(i).IDPIECE +'">'+ res.rows.item(i).LIBPIECE+'</option>');
                } 

            },
            function(error){
                alertify.alert("Liste pièce d'identité non chargée.");
            });
    });
}

function remplissageMPaie(){}

function replissagePeriodicite(){}

function loadParamEpargne(){
    // Chargement Frais d'entrée
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 1], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Frais_adhesion').innerHTML = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Frais d'adhésion non chargée.");
            });
    });

    //Chargement Taux de gestion
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 2], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Taux_gestion').innerHTML = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Taux de gestion non chargée.");
            });
    });

    //Chargement Taux Encours gérés
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 3], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Taux_encours').innerHTML = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Taux Encours non chargée.");
            });
    });

    //Chargement Dévise Pays
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PAYS WHERE CODEPAYS = ?', [225], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Devise_pays').innerHTML = res.rows.item(0).DEVISEPAYS;
            },
            function(error){
                alertify.alert("Dévise pays non chargée.");
            });
    }); 

    //Chargement Nom produit
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM PRODUIT WHERE CODEPRODUIT = ?', [codeProduit], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                document.getElementById('Nom_produit').innerHTML = res.rows.item(0).LIBPRODUIT;
            },
            function(error){
                alertify.alert("Nom produit non chargée.");
            });
    });

    //Chargement Age Min
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 5], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                ageMin = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Age Minimal non chargée.");
            });
    });

    //Chargement Age Max
    db.readTransaction(function (txn) {
        txn.executeSql('SELECT * FROM UTILISER WHERE CODEPRODUIT = ? AND IDPARAM = ?', [codeProduit, 6], 
            function (tx, res) {
                //alert(JSON.stringify(res.rows));
                ageMax = res.rows.item(0).VALEURPARAM;
            },
            function(error){
                alertify.alert("Age Maximal non chargée.");
            });
    });
}

function pieceJointe1(){
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess1, onFail, { quality: 50, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}


function pieceJointe2(){
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess2, onFail, { quality: 50, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}

function onPhotoDataSuccess1(imageData) {
    // Uncomment to view the base64-encoded image data
    // console.log(imageData);

    // Get image handle
    //
    var smallImage = document.getElementById('Pj1');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = "data:image/jpeg;base64," + imageData;

    document.getElementById("delete_Pj1").style.display="block";
}

function onPhotoDataSuccess2(imageData) {
    // Uncomment to view the base64-encoded image data
    // console.log(imageData);

    // Get image handle
    //
    var smallImage = document.getElementById('Pj2');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = "data:image/jpeg;base64," + imageData;

    document.getElementById("delete_Pj2").style.display="block";
}

function deletePieceJointe1(){

     var smallImage = document.getElementById('Pj1');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = imageDefaultData;

    document.getElementById("delete_Pj1").style.display="none";
}

function deletePieceJointe2(){

     var smallImage = document.getElementById('Pj2');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    smallImage.src = imageDefaultData;

    document.getElementById("delete_Pj2").style.display="none";
}

// Called if something bad happens.
//
function onFail(message) {
    alertify.alert('AZER06: ' + message);
}

/*function openCamera(selection) {
    var srcType = Camera.PictureSourceType.CAMERA;
    var options = setOptions(srcType);
    var func = createNewFileEntry;

    if (selection == "camera-thmb") {
        options.targetHeight = 200;
        options.targetWidth = 200;
    }

    navigator.camera.getPicture(function cameraSuccess(imageUri) {
        document.getElementById("Pj1").src=imageUri;
        // Do something

    }, function cameraError(error) {
        console.debug("Unable to obtain picture: " + error, "app");

    }, options);
}

function openFilePicker(selection) {
    var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
    var options = setOptions(srcType);
    var func = createNewFileEntry;

    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        // Do something

    }, function cameraError(error) {
        console.debug("Unable to obtain picture: " + error, "app");

    }, options);
}*/

function ajoutBeneficiaire(){
    alert();
    // Si B1 actif - B2 inactif - B3 inactif (condition effectuée sur entête B1)
    if(enteteBlockBeneficiare1.style.display=="none"){
        //Activer Entête B1 - B2
        enteteBlockBeneficiare1.style.display="block";
        blockBeneficiaire2.style.display="block";
    }
    // Si B1 actif - B2 actif
    else {
        if((enteteBlockBeneficiare1.style.display=="block") && (blockBeneficiaire2.style.display=="block")){
            //Activer B3
            blockBeneficiaire3.style.display="block";
            iconAjoutBeneficiare.style.display="none";
        }
    }
}