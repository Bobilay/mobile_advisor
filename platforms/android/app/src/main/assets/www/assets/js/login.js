//Authentification
function login(){

  //alert("ok - login");

  //var db = sqlitePlugin.openDatabase('mydbapp.db');

  db.readTransaction(function (txn) {
    var login = $("#Login").val();
    var mdp = $("#Password").val();
    //alert(login);
    //alert(mdp);
    var executeQuery = "SELECT * FROM AGENT WHERE CODEAGENT = ? AND MDPAGENT = ?";
    txn.executeSql(executeQuery, [login,mdp], 
      function (tx, res) {
        if(res.rows.length==1){
          //alert("ok");
          //alert(JSON.stringify(res.rows.item(0))); 
          insertLog();
        }else{
          //alert("AZER02: Code ou mot de passe incorrects.");
          alertify.alert("Code ou mot de passe incorrects.");
          //alert(JSON.stringify(res.rows.item(0))); 
        }
      },
      function(error){
        //alert("AZER02: Code ou mot de passe incorrects.");
        alertify.alert("Code ou mot de passe incorrects.");

      });
  });
}


function insertLog(){
  //alert("ok - log");

  db.transaction(function (txn) {
    var codeOP ="OP001";
    //alert(codeOP);
    var codeAG = $("#Login").val();
    //alert(codeAG);
    var today = new Date();
    var datSys = formatDate(today.toUTCString());
    //alert(datSys);

    var executeQuery = "INSERT INTO EFFECTUER (CODEOPERATION,CODEAGENT,DATEHEUREOPEF) VALUES (?,?,?)";
    txn.executeSql(executeQuery, [codeOP,codeAG,datSys], 
      function (tx, res) {  
        document.location.href = "accueil.html"; 
        /*db.readTransaction(function (txn) {
            txn.executeSql('SELECT * FROM EFFECTUER', [], function (tx, res) {
              alert('CONTROLE:');
              alert(JSON.stringify(res));
              document.location.href = "accueil.html";  
            });
          });
        */

      },
      function(error){
        //alert("AZER03: Connexion non enregistrée. Prière vous reconnecter.");
        //alert("error: "+JSON.stringify(error)); 
        alertify.alert("Connexion non enregistrée. Prière vous reconnecter.");
      });
  });
}

function formatDate(date) {
    var d = new Date(date),
        second = '' + d.getSeconds(),
        min = '' + d.getMinutes(),
        hour = '' + d.getHours(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (min.length < 2) min = '0' + min;
    if (second.length < 2) second = '0' + second;

    var infoDate = [year, month, day].join('-');
    var infoHour = [hour, min, second].join(':');
    return [infoDate, infoHour].join(' ');
}

