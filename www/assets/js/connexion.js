/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        function copyDatabaseFile(dbName) {
          var sourceFileName = cordova.file.applicationDirectory + 'www/' + dbName;
          var targetDirName = cordova.file.dataDirectory;

          return Promise.all([
            new Promise(function (resolve, reject) {
              resolveLocalFileSystemURL(sourceFileName, resolve, reject);
            }),
            new Promise(function (resolve, reject) {
              resolveLocalFileSystemURL(targetDirName, resolve, reject);
            })
          ]).then(function (files) {
            var sourceFile = files[0];
            var targetDir = files[1];
            return new Promise(function (resolve, reject) {
              targetDir.getFile(dbName, {}, resolve, reject);
            }).then(function () {
              alert("file already copied");
            }).catch(function () {
              alert("file doesn't exist, copying it");
              return new Promise(function (resolve, reject) {
                sourceFile.copyTo(targetDir, dbName, resolve, reject);
              }).then(function () {
                alert("database file copied");
              });
            });
          });
        }

        copyDatabaseFile('mydbapp.db').then(function () {
          // success! :)
          var db = sqlitePlugin.openDatabase('mydbapp.db');
          db.readTransaction(function (txn) {
            txn.executeSql('SELECT * FROM AGENT WHERE CODEAGENT =AG2', [], function (tx, res) {
              alert('Successfully read from pre-populated DB:');
              alert(JSON.stringify(res.rows.item(0))); 
            });
          });
        }).catch(function (err) {
          // error! :(
          alert(err);
        });
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        setInterval(function(){
	        checkConnection();
	    }, 2000);
    }
};

app.initialize();

function checkConnection() {
    var networkState = navigator.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
 
    //alert('Connection type: ' + states[networkState]);

    if(networkState=="none"){
        document.getElementById("statConnexion").style.display = "block";
    }else{
      if((networkState=="unknown")||(networkState=="CELL_2G")||(networkState=="CELL")){
        document.getElementById("statConnexion").style.display = "block";
      }else{
        document.getElementById("statConnexion").style.display = "none";
      }
    }
}


 
